import { Injectable } from '@angular/core';
import { of, Observable, throwError } from 'rxjs';

import { User } from '@data/schema/user';

interface LoginContextInterface {
  username: string;
  password: string;
  token: string;
}

const defaultUser = {
  username: 'Mark',
  password: 'password12345',
  token: 'cxvzcxvxczvczv'
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: string;

  login(loginContext: LoginContextInterface): Observable<User> {
    const isDefaultUser =
      loginContext.username === defaultUser.username &&
      loginContext.password === defaultUser.password;

    if (isDefaultUser) {
      return of(defaultUser);
    }

    return throwError('Invalid credentials');
  }

  logout(): Observable<boolean> {
    console.warn('Logging out...');
    this.token = null;
    localStorage.removeItem('auth-token');
    return of(true);
  }
}
